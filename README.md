## MediaWiki Bugzilla Extension

This extension allows to list bugs from Bugzilla in a Wiki page.

## Configuration

```php
wfLoadExtension('Bugzilla');
$wfBugzillaRestApiUrl = 'https://bugs.kde.org/rest';
```

## Example usage

```
{{#tag:bugzilla|
{
"product": "umbrello",
"status" : "CONFIRMED",
"severity" : "wishlist",
"target_milestone": "2.19 (KDE Applications 16.04",
"include_fields" : [ "id", "product", "summary", "assigned_to"]
}|title=TODO|class=table-bugs-todo}}
```

This will create the following get request to the bugzilla API:

https://bugs.kde.org/rest/bug?product=umbrello&status=CONFIRMED&severity=wishlist&target_milestone=2.19+%28KDE+Applications+16.04%29&include_fields=id%2Cproduct%2Csummary%2Cassigned_to&

## License

This project is licensed under GPL2 or later

## Maintainer

The maintainer of this project is Carl Schwan.
